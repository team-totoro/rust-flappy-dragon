use:: bracket_lib::prelude::*;

const SCREEN_WIDTH:i32 = 80;
const SCREEN_HEIGHT:i32 = 50;
const FRAME_DURATION: f32 = 30.0;

enum GameMode {
    Menu,
    Playing,
    End,
}

struct Player {
    x: i32,
    y: i32,
    velocity: f32,
}

const TERMINAL_VELOCITY:f32 = 2.0;
const GRAVITY_ACCELERATION:f32 = 0.5;
const FLAP_FORCE:f32 = -3.0;
const PLAYER_DEFAULT_START_X:i32 = 20;
const PLAYER_DEFAULT_START_Y:i32 = 25;
const PLAYER_SPEED:i32 = 1;

impl Player{
    fn new(x: i32,y:i32) -> Self {
        Player {
            x,y,velocity: 0.0,
        }
    }
    fn render(&mut self,ctx: &mut BTerm){
        ctx.set(
            0,
            self.y,
            YELLOW, //could use RGB::from_hex() to specify rgb from 00 to FF
            BLACK,  //could use RGB::from_u8() to specify rgb from 0 to 254
            to_cp437('@')
        );
    }
    fn gravity_and_move(&mut self){
        if self.velocity < TERMINAL_VELOCITY {
            self.velocity += GRAVITY_ACCELERATION;
        }
        self.y += self.velocity as i32;
        self.x += PLAYER_SPEED;
        if self.y < 0 {
            self.y = 0;
        }
    }
    fn flap(&mut self){
        self.velocity = FLAP_FORCE;
    }
}
const GAP_MIN:i32 = 10;
const GAP_MAX:i32 = 40;
const OBSTACLE_SIZE_MIN:i32 = 2;
const OBSTACLE_SIZE_MAX:i32 = 20;
struct Obstacle {
    x: i32,
    gap_y:i32,
    size: i32,
}
impl Obstacle{
    fn new(x:i32,score:i32)->Self{
        let mut random = RandomNumberGenerator::new();
        Obstacle {
            x,
            gap_y: random.range(GAP_MIN,GAP_MAX),
            size: i32::max(OBSTACLE_SIZE_MIN,OBSTACLE_SIZE_MAX - score)
        }
    }
    fn render(&mut self, ctx: &mut BTerm, player_x : i32){
        let screen_x = self.x - player_x;
        let half_size = self.size / 2;

        //draws the top half
        for y in 0..self.gap_y - half_size {
            ctx.set(
                screen_x,
                y,
                RED,
                BLACK,
                to_cp437('|'),
            );
        }
        //draws the bottom half
        for y in self.gap_y + half_size..SCREEN_HEIGHT {
            ctx.set(
                screen_x,
                y,
                RED,
                BLACK,
                to_cp437('|'),
            );
        }
    }
    fn hit_obstacle(&self, player: &Player) -> bool {
        let half_size = self.size / 2;
        let does_x_match = player.x == self.x;
        let player_above_gap = player.y < self.gap_y - half_size;
        let player_below_gap = player.y > self.gap_y + half_size;
        does_x_match && (player_above_gap || player_below_gap)
    }
}

struct State {
    mode: GameMode,
    player: Player,
    frame_time:f32,
    obstacle: Obstacle,
    score: i32,
}
impl State {
    fn new() -> Self {
        State{
            mode: GameMode::Menu,
            player: Player::new(PLAYER_DEFAULT_START_X,PLAYER_DEFAULT_START_Y),
            frame_time: 0.0,
            obstacle: Obstacle::new(SCREEN_WIDTH,0),
            score:0,
        }
    }
    fn play(&mut self,ctx: &mut BTerm){
        ctx.cls_bg(NAVY);
        ctx.print(0,0,"Press SPACE to flap.");
        ctx.print(0,1,format!("Score: {}",self.score));
        //just to make the game clunkier but playable
        self.frame_time += ctx.frame_time_ms;
        if self.frame_time > FRAME_DURATION {
            self.frame_time = 0.0;
            self.player.gravity_and_move();
        }

        if let Some(VirtualKeyCode::Space) = ctx.key{
            self.player.flap();
        }

        self.player.render(ctx);
        self.obstacle.render(ctx, self.player.x);

        if self.player.x > self.obstacle.x {
            self.score += 1;
            self.obstacle = Obstacle::new(
                self.player.x + SCREEN_WIDTH,self.score
            );
        }
        if self.player.y > SCREEN_HEIGHT || self.obstacle.hit_obstacle(&self.player){
            self.mode = GameMode::End;
        }
    }
    fn main_menu(&mut self,ctx : &mut BTerm){
        ctx.cls();
        ctx.print_centered(5, "Welcome to flappy dragon");
        ctx.print_centered(8, "(P) Play Game");
        ctx.print_centered(9, "(Q) Quit Game");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => ctx.quitting = true,
                _ => {}
            }
        }
    }
    fn dead(&mut self,ctx : &mut BTerm){
        ctx.cls();
        ctx.print_centered(5, "You are dead!");
        ctx.print_centered(6, &format!("You earned {} points",self.score));
        ctx.print_centered(8, "(P) Play Again");
        ctx.print_centered(9, "(Q) Quit Game");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.restart(),
                VirtualKeyCode::Q => ctx.quitting = true,
                _ => {}
            }
        }
    }
    fn restart(&mut self){
        self.player = Player::new(PLAYER_DEFAULT_START_X,PLAYER_DEFAULT_START_Y);
        self.frame_time = 0.0;
        self.mode = GameMode::Playing;
        self.score = 0;
        self.obstacle = Obstacle::new(SCREEN_WIDTH,self.score);
    }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm){
        match self.mode {
                GameMode::Menu => self.main_menu(ctx),  
                GameMode::End => self.dead(ctx),        
                GameMode::Playing => self.play(ctx),    
        }
    }
}

fn main() -> BError{
    let context = BTermBuilder::simple80x50()
    .with_title("Flappy Dragon")
    .build()?;
    main_loop(context, State::new())
}
